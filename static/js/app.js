(function() {
    'use strict';

    var app = angular.module('mathspaceApp', [
        'ngResource'
    ]);

    app.factory('AStar', ['$resource',
        function ($resource) {
            return $resource('/api/astar', {}, {
                findPath: {
                    method: 'post',
                    transformResponse: function (data) {
                        return JSON.parse(data);
                    }
                }
            });
        }
    ]);

    app.directive('grid', ['$timeout', 'AStar',
        function($timeout, AStar) {
            return {
                restrict: 'A',
                templateUrl: 'static/templates/grid.html',
                link: function(scope, element) {
                    scope.restrict = true;
                    scope.grid = [
                        ['46B', 'E59', 'EA', 'C1F', '45E', '63'],
                        ['899', 'FFF', '926', '7AD', 'C4E', 'FFF'],
                        ['E2E', '323', '6D2', '976', '83F', 'C96'],
                        ['9E9', 'A8B', '9C1', '461', 'F74', 'D05'],
                        ['EDD', 'E94', '5F4', 'D1D', 'D03', 'DE3'],
                        ['89', '925', 'CF9', 'CA0', 'F18', '4D2']
                    ];

                    scope.isHex = true;

                    scope.flipInput = function() {
                        scope.convertGrid(scope.grid);
                    };

                    scope.convertGrid = function(grid) {
                        _.each(grid, function(row, row_index) {
                            _.each(grid[row_index], function(tile, column_index) {
                                grid[row_index][column_index] = scope.isHex ? parseInt(tile, 16) : tile.toString(16).toUpperCase();
                            });
                        });
                    };

                    scope.submit = function() {
                        var tmpGrid = _.cloneDeep(scope.grid),
                            timeout = 0;

                        if (scope.isHex) {
                            scope.convertGrid(tmpGrid);
                        }

                        AStar.findPath({
                            grid: tmpGrid,
                            restrict: scope.restrict
                        }, function(response){
                            $('.tile').removeClass('active');
                            _.each(response.path, function(tile) {
                                $timeout(function(){
                                    $('.tile_' + tile[0] + '_' + tile[1]).addClass('active');
                                }, timeout);
                                timeout += 250;
                            });
                        });
                    };
                }
            };
        }
    ]);
})();