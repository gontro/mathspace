# Mathspace Technical Challenge - Frontend

A solution to problem #1 in the Mathspace Technical Challenge.

## Technology stack

* NodeJS and express server
* AngularJS
* Bower for external dependencies

## Getting up and running

This setup was built on a Ubuntu 14.04.1 server.  The following will get you setup to develop.

```
$ sudo apt-get install nodejs npm
$ sudo npm install -g bower
$ git clone https://gontro@bitbucket.org/gontro/mathspace.git
$ cd white-agency
$ npm install
$ bower install
```

This will install all the npm and bower dependencies.  To run the server, execute this command.

```
$ node server.js
```

And in another window run this command once the api is running.

```
$ node proxy.js
```

And that's it!  Browse to http://localhost:3000 to view.