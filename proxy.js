(function(){
    'use strict';

    var http                = require('http'),
        httpProxy           = require('http-proxy');

    var proxy = httpProxy.createProxyServer({});

    var server = http.createServer(function(req, res) {
        if (req.url.substring(0,4) === '/api') {
            proxy.web(req, res, { target: 'http://localhost:8000' });
            proxy.on('error', function(err) {
                console.log('Error: oops.. an error occurred connecting to the Mathspace API');
                console.log('Are you running locally or from AWS?');
                console.log(err);
            });

        } else {
            proxy.web(req, res, { target: 'http://localhost:3001' });

            proxy.on('error', function(err) {
                console.log('Error: oops.. an error occurred connecting to the Mathspace node.js server');
                console.log('Please check that you are running \'node server.js\' or \'nodemon server.js\' in the Mathspace directory');
                console.log(err);
            });
        }
    });

    console.log();
    console.log('Mathspace Proxy listening on port 3000');
    server.listen(3000);
})();