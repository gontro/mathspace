(function(){
    'use strict';

    var express = require('express');

    var app = express();

    app.use(express.static(__dirname));

    var server = app.listen(3001, function() {
        console.log('Express server listening on port %d', server.address().port);
    });
})();
